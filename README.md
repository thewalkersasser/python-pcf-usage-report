# PCF-Usage-Report
This script iterates over each org and each space within them to calculate the number of active app instances and present service instances in PCF.

- Output is currently in JSON format but could easily be adapted
- The script uses env variables for credentials

## Env Variables
- EMAIL - LDAP ID (*Ex: ID12345*)
- PASSWORD - PCF Password (*Most likely same as LDAP password*)
- ORG - When logging in initially, an org must be targeted, this can be any valid organization name in PCF (*Ex: Sandbox*)
- SPACE - When logging in intially, a space within an org must be targeted, this can be any valid space within the organization defined in the ORG env variable (*Ex: Test*)
- API_ENDPOINT - the url being used to connect to PCF

## Using a temporary text file to guide the parsing
When using a cf cli command, like 'cf orgs' for example, the output comes in a format like so:

Getting orgs as ID12345...

name\n
\nOrg1
\nOrg2
\nOrg3

The approach taken to get the org names in a list is as follows:

1. Output the command to a txt file
2. Read the txt file as a string
3. Split based on \n characters ('Getting orgs as ID12345', '', 'name', 'Org1', 'Org2', 'Org3', '')
4. Loop through list to get rid of '' entries ('Getting orgs as ID12345', 'name', 'Org1', 'Org2', 'Org3')
5. Drop 1st and 2nd entries of the list ('Org1', 'Org2', 'Org3')

This process is used throughout the script to acquire lists of strings for orgs, spaces, apps, and services in PCF. See the comments in any of the methods for more information.

## Data 
The directory dictionary stored as a class variable in the PCF_Report object contains all the information about each app instance and service instance across orgs and spaces. So, if one wanted to find other things in PCF across orgs and spaces this could be done by altering the generate_report method or adding a custom one.

` 
{   "Org": {
        "Space": {
            "App instances": integer,
            "Service instances": integer,
            "Services": {
                "name": "service"
            }
        }
    }
}
`

## Running Locally
Follow these steps to run locally:

1. Clone the project
2. Create a .env file in the directory where main.py is
3. Add the required env variables (see 'Env Variables')
4. Run *pip install -r requirements.txt*
5. Have fun and go crazy

